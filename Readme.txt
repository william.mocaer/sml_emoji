5 notebooks sont fournis, � executer dans cet ordre.
- 0) Brut To Structured : transformation des donn�es brutes en donn�es lisibles par pandas, s�paration des tweets et emojis
- 1) PreprocessData : Visualisation, S�paration des donn�es, pr�processing du texte, export des features annexes,
exporte les fichiers n�cessaires dans le dossier "ProcessedData", ils seront lus par les deux fichiers suivant.
- 2) MainTfIdf : Classification avec repr�sentation BOW et TF/IDF
- 2) MainWord2Vec : Classification avec word2vec
- 3) HandTesting : Testez � la main avec vos propres tweets.

le mod�le de word2vec est � t�l�charger ici : http://nlp.stanford.edu/data/glove.twitter.27B.zip, 
s�lectionnez le mod�le � 100 dimensions, ajouter "1193514 100" sur la premi�re ligne du fichier. 
� mettre dans le Dossier "Models".

Le mod�le le plus performant pour les 20 emojis (DNN) a �t� retir� du gitignore afin que vous puissez tester sans tout r�executer.

History contient les historiques des apprentissages avec les NN
Best params contient les meilleurs paramametres des SVM enregistr� avec le GridSearch
Log contient tous les r�sultats enregistr�s au cours du projet.
venv contient les requirements pour executer le projet (voir Readme_InstallVenv)
BrutData contient les donn�es brutes